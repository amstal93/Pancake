FROM python:3.6
MAINTAINER Mike Gregory <mike@mgregory.me>

RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository 'deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main' \
    && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
    && apt-get update \
    && apt-get install -y \
        sudo \
        postgresql-10 \
        postgresql-client \
    && pip install migra[pg]

COPY generate migrate watch persistent /usr/bin/
RUN chmod +x /usr/bin/generate /usr/bin/migrate /usr/bin/watch /usr/bin/persistent

WORKDIR /data
CMD ["persistent"]
